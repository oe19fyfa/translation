# Reactive load distributor ok.ru/music

The article is a transcript of Vadim Zesko's report on Joker 2018.

## Statistics

First, a few words about OK social network. This is a giant service that is used by more than 70 million users. They are served by 7 thousand machines in 4 data centers. Recently, on traffic, they broke through the 2 TB / s mark. The most loaded services serve up to 100,000 requests per second from a four core node. At the same time, almost all services are written in Java. 

One of the most popular services of the social network - "Music". In it, users can upload their tracks, buy and download music in different qualities. But the main purpose of the service, of course, is to play music.

The distributor of music is engaged in data transfer to user players and mobile applications. It can be caught in the web inspector, if you look at requests to the domain musicd.mycdn.me. API distributor is extremely simple. It responds to HTTP requests `GET`and issues the requested track range. 

![img](assets/j9vazesvr2fuqtpvhec0uj7gbmq.png)

At peak load reaches 100 Gb / s through half a million connections. In fact, the music distributor is a cache front-end in front of our internal storage of tracks. According to the statistics, about 140 tracks cover 10% of all auditions in a day. Thus, if we want our caching server to have a cache hit of at least 90%, then we need to put half a million tracks into it. 95% - almost a million tracks.

![img](assets/q6jvpcopw2t0xt29sl84lwp1gkg.png)

## Distributor Requirements

We wanted one node to hold 100 thousand connections. These are slow client connections: a bunch of browsers and mobile applications via networks with varying speeds. At the same time, the service, like all our systems, must be scalable and fault-tolerant. 

First of all, we need to scale the bandwidth of the cluster in order to keep up with the growing popularity of the service and be able to give more and more traffic. You also need to be able to scale the total capacity of the cluster cache, because cache hit directly depends on it and so the share of requests that will go into the track storage.

Today it is necessary to be able to scale any distributed system horizontally, that is, add machines and data centers. But we also wanted to implement vertical scaling. Our typical modern server contains 56 cores, 0.5-1 TB of RAM, a 10 or 40 Gbit/s network interface and a dozen SSD disks. 

Speaking of horizontal scalability, an interesting effect arises: when you have thousands of servers and tens of thousands of disks, something constantly breaks. Failure of the discs is a routine, we change them for 20-30 pieces a week.  

When we design any systems, we know that they will break sooner or later. Therefore, we always carefully study failure scenarios of all system components. The main way to deal with failures is to backup using data replication: several copies of data are stored on different nodes. 

We also reserve network bandwidth. This is important, because if a component of the system fails, the load on the other components should not be allowed to collapse the system.  

## Balancing

First you need to balance user requests between data centers. Additionally, balancing also needs to be done between nodes inside data centers. Distribution of requests between the nodes is implemented using weights. This is useful when a new version of the service is posted and a new node needs to be introduced smoothly into the rotation. Also, weights are very helpful in load testing: we increase the weight and get much more workload on the node. And when the node fails under load, we quickly zero the weight and remove it from the rotation. This approach allows us to find out the limits of the tested node. 

Here is the query path from the user to the node, which will return the data.

![img](assets/uutzuf3bnfpetnns91ca-rcf5mc.png)

The user logs into his account and receives the track url: `musicd.mycdn.me/v0/stream?id=...`

To get the IP address from the host name in the URL, the client contacts our GSLB DNS, which knows about all our data centers and CDN platforms. GSLB DNS gives the client the IP address of the balancer of one of the data centers, and the client establishes a connection with it. The balancer knows about all the nodes inside the data centers and their weight. He on behalf of the user establishes a connection with one of the nodes. We use L4 balancers based on NFWare. The node gives the user data directly, bypassing the balancer. In services like a distributor, outgoing traffic significantly exceeds incoming traffic.

If a data center fails, GSLB DNS detects this and promptly takes it out of rotation: it stops giving users the IP address of the balancer of this data center. If a node fails in a data center, then its weight is reset, and the balancer inside the data center stops sending requests to it. 

Now consider the balancing of tracks on the nodes inside the data center. We will consider data centers as independent autonomous units, each of them will live and work, even if all the others died. Tracks need to be balanced on the machines evenly, so that there is no load imbalance, and replicate them to different nodes. If one node fails, the load should be distributed equally among the rest. 

This problem was solved by use of consistent hashing. The entire possible range of track identifier hashes is wrapped in a ring, and then each track is mapped to a point on this ring. Then we more or less evenly distribute the ring ranges between the nodes in the cluster. The nodes that will store the track are selected by hashing the tracks to a point on the ring and moving clockwise. 

![img](assets/qrtyxfbchjhlp5taqfs0fbicnk0.jpeg)

But this scheme has a drawback: in the event of a failure of the N2 node, for example, its entire load will fall on the next replica on the ring - N3. And if it does not have a double performance margin - and this is not economically justified - then, most likely, N3 will fall. The load will be transferred to N4 and so on - a cascade failure will occur along the entire ring.

This drawback can be mitigated by dividing the ring into a much larger number of ranges, which are randomly scattered around the ring. In the example below, each node is responsible for two ranges. If one of the nodes fails, its entire load will fall not on the next node around the ring, but will be distributed between two other nodes of the cluster.

![img](assets/kcuf5-1mmekp60d5emkp5ckkgps.png)

The ring is calculated on the basis of a small set of parameters algorithmically and deterministically at each node. That is, we do not store it in some kind of config. We have more than one hundred thousand of these ranges in production, and in case of failure of any of the nodes, the load is distributed absolutely evenly among all other live nodes. 

How does track output to a user in a consistent hashing system look like?

The user through the L4-balancer falls on a random node. The choice of the node is random, because the balancer knows nothing about the topology. But then every node in the cluster knows about it. The node that receives the request determines whether it is a replica for the requested track. If not, it switches to proxy mode to one of the nodes, establishes a connection with it, and the new node searches for data in its local storage. If the track is not there, the node pulls it out of the track repository, saves it to the local repository and gives it to the proxy, which forwards the data to the user.  

![img](assets/c5puwlig-lfxy20leny4y4utqig.png)

If a node fails, then the proxy-node knows about all the other nodes for this track, it will establish a connection with another live node and receive data from it. So we guarantee that if a user has requested a track and at least one node is alive, he will receive an answer.

## Request processing inside the data cluster

![img](assets/2mupob-rqyqsbic50lc-qn2xwqe.png)



First, the request goes to the external API (we give everything via HTTPS). Further validation of the request is performed - signatures are checked. The request goes to the routing stage, where, based on the cluster topology, it is determined how the data will be given: either the current node is a replica for this track, or we will be proxying from another node. In the second case, the node through a proxy client establishes a connection to the replica-node via the internal HTTP API without checking signatures. The replica-node searches for data in the local storage, if it finds a track, it returns it from its disk; and if it does not find it, it pulls up tracks from the repository, caches it and gives away.

## Data storage on node

If each track is put in a separate file, then the overhead of managing these files will be huge. Even restarting the node and scanning the data on the disks will take minutes, if not tens of minutes. 

There are less obvious limitations in this scheme. For example, you can load tracks only from the very beginning. And if the user requested playback from the middle and there was a cache miss, then we will not be able to give a single byte until we load the data to the desired location from the track repository. Moreover, we can also store the tracks only as a whole, even if it is a gigantic audiobook, which is already abandoned in the third minute. It will still lie like a dead weight on the disk, waste expensive space and reduce the cache hit of this node.

Therefore, we do it in a completely different way: we split up tracks into blocks of 256 KB each, because this correlates with the size of the block in the SSD, and we are already operating with these blocks. On a disk in 1 TB 4 million blocks are located. Each disk in the node is an independent storage, and all blocks of each track are distributed across all disks. 

We did not immediately come to such a scheme, at first all the blocks of one track lay on one disc. But this led to severe load imbalances between the disks, since if one of the disks had hit a popular track, all requests for its data would fall on one disk. To avoid this, we distributed the blocks of each track across all discs, equalizing the load.   

## Index

But it is not enough to be able to store blocks, it is necessary to maintain an index from blocks of music tracks to blocks on a disk. 

![img](assets/me4y0bk_xbwco9r_5y6smyhmxmc.png)

It turned out quite compact, one index record takes up only 29 bytes. For a storage size of 10 TB, the index takes a little more than 1 GB. 

To the index, we formulated a number of requirements:

 - it should work quickly (preferably stored in RAM)
 - it should be compact and not take up space from page cache
 - the index must be persistent. If we lose it, we will lose information about where in the disk which track is stored, and this is equivalent to cleaning the disks. 
 - it would be desirable that the old blocks, which have not been addressed for a long time, were somehow supplanted, making room for more popular tracks. We chose the [LRU crowding policy](https://github.com/ben-manes/caffeine/wiki/Efficiency) : the blocks are crowded out once a minute, and we keep 1% of the blocks free
 - the index structure must be thread-safe, because we have 100 thousand connections per node

All these conditions are ideally satisfied `SharedMemoryFixedMap`from our open-source [one-nio](https://github.com/odnoklassniki/one-nio) library .

We put the index on `tmpfs`, which works quickly, but there is a nuance. When the machine restarts, `tmpfs` is lost, including the index. In addition, if our process collapses, it is unclear in what condition the index remained. Therefore, we make a snapshot of it once an hour. But this is not enough: since we use block crowding, we have to maintain [WAL](https://en.wikipedia.org/wiki/Write-ahead_logging) , in which we write information about the crowded blocks. The block entries in the snapshots and the WAL need to be ordered at restoration time. For this we use block generation. It plays the role of a global transaction counter and is incremented every time the index changes. Let's look at an example of how this works. 

For example, we have an index with three entries: two blocks of track # 1 and one block of track # 2.

![img](assets/l6ux512_lbpedki0bb3g0ouggau.png)

The snapshot-creating thread is awakened and starts iterating over the index: the first and second tuples fall into the snapshot. Then the crowding thread accesses the index, realizes that the seventh block has not been addressed for a long time, and decides to use it for something else. The process displaces the block and writes an entry to WAL. Then the crowding thread gets to block 9, sees that it, too, has not been contacted for a long time, and also marks him as crowded out. Here the user accesses the system and a cache miss occurs - a track is requested that we don’t have. We save the block of this track in our storage, overwriting the 9th block. In this case, the generation is incremented and becomes equal to 22. Next, the snapshot-creating thread is activated, which has not completed its work, reaches the last record and writes it to the cast. As a result, we have two live entries in the index, a snapshot and a WAL.

![img](assets/syvj0_-kknmn4i5pulf_p8bclc4.png)

When the current node falls, it will restore the initial state of the index as follows. First, we scan the WAL and build a map of dirty blocks. The map stores the mapping from the block number to the generation when this block was preempted.


  ![img](assets/9pg5gn-8atbixm4swmytkscxwpm.png)

After that, we begin to iterate over the snapshot, using the map as a filter. We look at the first record of the snapshot, it concerns the block number 3. He is not mentioned among the dirty, so he is alive and falls into the index. We get to block number 7 with the eighteenth generation, but the map of dirty blocks tells us that just in the 18th generation, the block was crowded out. Therefore, it does not enter the index. We reach the last entry, which describes the contents of the 9th block with 22 generation. This block is mentioned in the map of dirty blocks, but it has higher generation then the block ID in the dirty map. This means that it has been reused for new data and is indexed. 

## Optimizations

But that's not all, we go down deeper. 

Let's start with page cache. We were counting on it initially, but when we started to carry out load testing of the first version, [it turned](https://github.com/brendangregg/perf-tools/blob/master/fs/cachestat) out that page cache hit rate does not reach 20%. We assumed that the problem is in read ahead: we do not store files, but blocks, while serving a lot of connections, and in such a configuration it is efficient to work with the disk randomly rather than read ahead. Fortunately, there is a call in Linux [`posix_fadvise`](http://man7.org/linux/man-pages/man2/posix_fadvise.2.html)that allows you to tell the kernel how you are going to work with the file descriptor. Particularly, by passing the flag `POSIX_FADV_RANDOM`,  we can say that we don’t want to use read ahead. This system call is available through [one-nio](https://github.com/odnoklassniki/one-nio/blob/master/src/one/nio/os/Mem.java). In production, our cache hit is 70-80%. The number of physical reads from disks decreased by more than 2 times, the HTTP response delay decreased by 20%. 

Let's move on. The service has a rather big heap size. To make life easier for the processor's TLB caches, we decided to enable Huge Pages for our Java process. As a result, we received a noticeable profit for the time of garbage collection (GC Time / Safepoint Total Time is 20-30% lower), the load on the cores became more uniform, but we did not notice any effect on the HTTP latency charts.  

## Incident Analysis

Analysis of problems in such distributed systems is difficult, because the user-request goes through many stages and crosses the borders of the nodes. In the case of CDN, everything becomes even more complicated, because for CDN the upstream is the home data center. Moreover, the system serves hundreds of thousands of user connections. It is quite difficult to understand at what stage the problem arises with the processing of a particular user's request. 

We simplify our lives as follows. At the entrance to the system, we mark all requests with a tag by analogy with [Open Tracing](https://opentracing.io/) and [Zipkin](https://zipkin.io/). The tag includes the user ID, the request and the requested track. This tag inside the pipeline is transmitted with all the data and requests related to the current connection, and between nodes is transmitted as an HTTP header and is restored by the receiving party. When we need to deal with the problem, we turn on debugging, log the tag, find all the records related to a particular user or track, aggregate and find out how the request was processed all the way through the cluster.

## Sending Data

Consider a typical scheme for sending data from disk to socket. It seems nothing complicated: select the buffer, read from the disk to the buffer, send the buffer to the socket.

```
ByteBuffer buffer = ByteBuffer.allocate(size);
int count = fileChannel.read(buffer, position);
if (count <= 0) {
   // ...
}
buffer.flip();
socketChannel.write(buffer);
```

One of the problems with this approach is that two hidden data copies are hidden here:

- when reading from a file using `FileChannel.read()`, copying from kernel space to user space occurs
- and when we send data from the buffer to the socket using `SocketChannel.write()`, copying from user space to the kernel space takes place

Fortunately, there is a call in Linux  [`sendfile()`](http://man7.org/linux/man-pages/man2/sendfile.2.html), which allows you to ask the kernel to send data from a file to the socket from a certain offset directly, bypassing copying to user space. And of course, this call is available through  [one-nio](https://github.com/odnoklassniki/one-nio). On the load tests  we initiated user traffic to one node and forced it to be proxied by the neighbor node, which uploaded data only through  `sendfile()` - the processor load at processing of 10 Gbit / s was close to 0. 

But in case of user-space SSL sockets we cannot use `sendfile()` and we have nothing left but to send data from the file through the buffer. And here is another surprise. If you delve into the source-code of `SocketChannel` and `FileChannel`, or use [Async Profiler](https://github.com/jvm-profiling-tools/async-profiler) and profile the system in the process of data return then sooner or later you get to the class `sun.nio.ch.IOUtil` which contains all `read()`, and `write()` calls on these channels. There you will find following code:

```
ByteBuffer bb = Util.getTemporaryDirectBuffer(dst.remaining());
try {
    int n = readIntoNativeBuffer(fd, bb, position, nd);
    bb.flip();
    if (n > 0)
        dst.put(bb);
    return n;
} finally {
    Util.offerFirstTemporaryDirectBuffer(bb);
}
```

This is a pool of native buffers. When you read from a file into `ByteBuffer` heap, first the standard library takes a buffer from this pool, reads data into it, then copies it into your heap `ByteBuffer`, and returns the native buffer back to the pool. When writing to a socket, the same thing happens. 

Controversial scheme. Here [one-nio](https://github.com/odnoklassniki/one-nio) comes to the rescue again . We create an allocator `MallocMT`- in fact, this is a memory pool. If we have SSL and we have to send data through a buffer, then we allocate the buffer outside of the Java heap, wrap it in `ByteBuffer`, read it without unnecessary copies from `FileChannel`this buffer, and write to the socket. And then return the buffer to the allocator.  

```
final Allocator allocator = new MallocMT(size, concurrency);

int write(Socket socket) {
    if (socket.getSslContext() != null) {
        long address = allocator.malloc(size);
        ByteBuffer buf = DirectMemory.wrap(address, size);
        int available = channel.read(buf, offset);
        socket.writeRaw(address, available, flags);
```

## 100,000 connections per node

But the success of the system is not guaranteed by the reasonableness of implementation at the lower levels. There is another problem here. The pipeline on each node serves up to 100 thousand simultaneous connections. How to organize calculations in such a system? 

The first thing that comes to mind is to create a flow of execution for each client or connection, and there we can perform the stages of the pipeline one after another. But with such a scheme, the costs of context switches and thread stacks will be excessive, since we are talking about the distributor and there are a lot of threads. Therefore, we went a different way.  

![img](https://habrastorage.org/webt/mt/vg/eo/mtvgeoszcpcv6zpc-qi_rbqeyhy.png)

For each connection, a logical pipeline is created, which consists of stages interacting with each other asynchronously. Each stage has its queue, which stores incoming requests. For the execution of the stages, small common thread pools are used. If we need to process the message from the request queue, we take the stream from the pool, process the message, and return the stream to the pool. With such a scheme, data is pushed from storage to the client.

But such a scheme has its flaws. Backends work much faster than user connections. When data passes through a pipeline, it accumulates at the slowest stage, i.e. at the stage of writing blocks to the client connection socket. Sooner or later it will lead to the collapse of the system. If you try to limit the queues at these stages, then everything will instantly stall, because the pipelines in the chain to the user's socket will be blocked. And since they use shared thread pools, they will block all threads in them. So, you need back pressure. 

For this, we used [reactive streams](http://www.reactive-streams.org/). The essence of the approach is that the subscriber controls the speed of data arrival from the publisher using demand. Demand means how much more data the subscriber is ready to process along with the previous demand, which it has already signaled. Publisher can only send data not exceeding the total accumulated demand at the moment minus the data already sent.

Thus, the system dynamically switches between push and pull modes. In push mode, a subscriber is faster than publisher, that is, publisher always has unsatisfied demand from a subscriber, but there is no data. As soon as the data appears, he immediately sends it to the subscriber. Pull mode occurs when publisher is faster than subscriber. That is, publisher would be happy to send data, but demand is zero. As soon as the subscriber reports that it is ready to process a little more, the publisher immediately sends a portion of the data.

Our pipeline turns into reactive stream. Each stage turns into a publisher for the previous stage and a subscriber for the next.  

The interface of ractive streams looks extremely simple. `Publisher`allows you to sign`Subscriber`, and he should only implement four handlers:

```
interface Publisher<T> {
    void subscribe(Subscriber<? super T> s);
}

interface Subscriber<T> {
    void onSubscribe(Subscription s);
    void onNext(T t);
    void onError(Throwable t);
    void onComplete();
}

interface Subscription {
    void request(long n);
    void cancel();
}
```

`Subscription` allows you to signal demand and cancel a subscription. Very simple, right?

As a data element, we are not passing arrays of bytes, but rather an abstraction like chunk. We do this in order to, if possible, avoid drawing data into heap. Chunk is a link to data with a very limited interface that only allows you to read data into `ByteBuffer`, write to a socket or to a file. 

```
interface Chunk {
    int read(ByteBuffer dst);
    int write(Socket socket);
    void write(FileChannel channel, long offset);
}
```

There are many implementations of chunks:

- The most popular, which is used in the case of cache hit and when transferring data from the disk, is the implementation on top of `RandomAccessFile`. The chunk contains only a link to the file, the offset in that file and the size of the data. It goes through the entire pipeline, reaches the user connection socket and turns into a call to `sendfile()`. In this scenario, memory is not consumed at all.
- In the case of cache miss, another implementation is used: we extract all blocks of the track from our storage and save it to disk. Chunk contains a reference to the socket (the client connection to the track storage) , position in the stream and data size.
- Finally, in the case of proxying, it is necessary to place the received block in heap. Here the chunk acts as a wrapper around `ByteBuffer`.

Despite the simplicity of this API, it should be thread safe, and most methods should be non-blocking. We chose a path in the spirit of the Typed Actor Model, inspired by [examples](https://github.com/reactive-streams/reactive-streams-jvm/tree/master/examples) from the [official repository](https://github.com/reactive-streams/reactive-streams-jvm) of [reactive streams](http://www.reactive-streams.org/). To make the method calls non-blocking, when we call the method, we take all the parameters, wrap the message, put it in the queue for execution, and return control. Messages from the queue are processed strictly sequentially.

Thus, we have a completely non-blocking implementation. The code is simple and consistent, without `volatile`, `Atomic*`, and contentions. For servicing 100,000 connections our whole system runs only 200 threads .

## Result

In production, we have 12 machines, while there is more than a double margin on connection throughput. Each machine normally gives up to 10 Gbit/s through hundreds of thousands of connections. We have provided scalability and fault tolerance. Everything is written in Java and [one-nio](https://github.com/odnoklassniki/one-nio). 

![1548425741587](C:\Users\Emakmel\AppData\Roaming\Typora\typora-user-images\1548425741587.png)

This is a graph to the first byte given to the user by the server. 99-percentile is less than 20 ms. The blue graph is the return to the user over HTTPS. The red graph is the return of data from the replica-node to the proxy via `sendfile() `HTTP. 

In fact, cache hit in production is 97%, so the charts describe the latency of our track repository, from which we pull up data in the case of cache misses, which is also good, given the petabytes of data. 

In this article, we wanted to demonstrate how we build highly scalable scalable systems that have both high speed and excellent fault tolerance. We hope we did it.